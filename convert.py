import json
import csv

# SOurce of JSON data: https://public.opendatasoft.com/explore/dataset/airbnb-listings/export/?disjunctive.host_verifications&disjunctive.amenities&disjunctive.features&fbclid=IwAR1LVZ56gLTY5BlcixOSYj8T2ogsJ2HpPx9vpkZCsM21Gzh9ojuS8uIrY74
with open("airbnb-listings.json", encoding="utf-8") as json_data:
  data = json.load(json_data)
  print(len(data))

employ_data = open('EmployData2.csv', 'w', newline='')
csvwriter = csv.writer(employ_data)
count = 0
for house in data:
  try:
    house
    row = house["fields"]
    if count == 0:
      #header = row.keys()
          header = ['country', 'city', 'transit', 'access', 'host_since', 'host_response_time', 'host_response_rate', 'state', 'property_type', 'room_type', 'accommodates', 'bathrooms', 'bedrooms', 'beds', 'bed_type', 'amenities', 'price', 'security_deposit', 'cleaning_fee', 'guests_included', 'extra_people', 'minimum_nights', 'maximum_nights', 'number_of_reviews', 'review_scores_rating', 'cancellation_policy' ]
      csvwriter.writerow(header)
    # create the csv writer object
    attribute = []
    for head in header:
      if head not in row.keys():
        attribute.append("None")
      else:
        attribute.append(row[head])
    print(attribute)
    csvwriter.writerow(attribute)
    count += 1
  except:
    continue